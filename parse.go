package ldb

import (
	"encoding/binary"
	"fmt"
	"hash/fnv"
	"os"
	"time"
)

// tstamp | key_sz | value_sz | value_pos | key |
// uint64 | uint16 | uint16 | uint32 | []byte |
const HINT_FILE_META_SIZE = 16

func createDeletedKvEntry(key, val []byte) []byte {
	tstamp := time.Now().Unix()
	buf := make([]byte, KV_ENTRY_META_SIZE+len(key)+len(val))
	binary.LittleEndian.PutUint64(buf[4:], uint64(tstamp))
	binary.LittleEndian.PutUint16(buf[12:], uint16(len(key)))
	binary.LittleEndian.PutUint16(buf[14:], uint16(len(val)))
	buf = append(buf, key...)
	buf = append(buf, val...)

	hasher := fnv.New32()
	hasher.Write(buf)
	crc := hasher.Sum32()
	binary.LittleEndian.PutUint32(buf[:], crc)
	return buf
}

func createKvEntry(key, val []byte) []byte {

	checksum := make([]byte, 4)
	buf := make([]byte, 12)

	tstamp := time.Time{}
	binary.LittleEndian.PutUint64(buf[:], uint64(tstamp.Unix()))
	binary.LittleEndian.PutUint16(buf[8:], uint16(len(key)))
	binary.LittleEndian.PutUint16(buf[10:], uint16(len(val)))
	buf = append(buf, key...)
	buf = append(buf, val...)

	hasher := fnv.New32()
	hasher.Write(buf)
	crc := hasher.Sum32()
	binary.LittleEndian.PutUint32(checksum[:], crc)

	checksum = append(checksum, buf...)
	return checksum
}

func getKvKeySize(buf *[]byte) int {
	keySz := binary.LittleEndian.Uint16((*buf)[12:])
	return int(keySz)
}

func getKvKey(buf *[]byte) []byte {
	ks := getKvKeySize(buf)
	keyStart := KV_ENTRY_META_SIZE
	keyEnd := keyStart + ks
	key := (*buf)[keyStart:keyEnd]
	return key
}

func getKvVal(buf *[]byte, keySz int) []byte {
	valSz := binary.LittleEndian.Uint16((*buf)[14:])
	valStart := keySz + KV_ENTRY_META_SIZE
	valEnd := valStart + int(valSz)
	val := (*buf)[valStart:valEnd]
	return val
}

func getKvValFromFile(file *os.File, key string, keydirEntry [KEYDIR_ENTRY_SIZE]byte) ([]byte, error) {
	_, valSz, valPos := getValueInfoFromKeydir(keydirEntry)
	_, err := file.Seek(int64(valPos), 0)
	if err != nil {
		return nil, fmt.Errorf("seek error, err=%s", err.Error())
	}
	buf := make([]byte, KV_ENTRY_META_SIZE+len(key)+int(valSz))
	_, err = file.Read(buf)
	if err != nil {
		return nil, fmt.Errorf("read error, err=%s", err.Error())
	}

	return getKvVal(&buf, len(key)), nil
}

func getKvTimestamp(buf *[]byte) time.Time {
	b := binary.LittleEndian.Uint64((*buf)[4:])
	return time.Unix(int64(b), 0)
}

func getKvValSize(buf *[]byte) uint16 {
	return binary.LittleEndian.Uint16((*buf)[14:])
}

// -------- HINT ENTRY STUFF -------------
// | tstamp | KeySz | ValSz | ValPos | Key |
//    8b       2b       2b      4b     []byte

func getHintEntryKey(buf *[]byte) []byte {
	return (*buf)[16:]
}

func getHintEntryValSize(buf *[]byte) int {
	return int(binary.LittleEndian.Uint16((*buf)[10:]))
}

func getHintEntryKeySize(buf *[]byte) int {
	return int(binary.LittleEndian.Uint16((*buf)[8:]))
}

func getHintEntryValPos(buf *[]byte) int {
	return int(binary.LittleEndian.Uint32((*buf)[12:]))
}

func getHintEntryTimestamp(buf *[]byte) uint64 {
	return binary.LittleEndian.Uint64((*buf)[:])
}

func createHintEntry(key string, valPos int64, valSz uint16) []byte {
	buf := make([]byte, HINT_FILE_META_SIZE)
	tstamp := time.Now().Unix()
	binary.LittleEndian.PutUint64(buf[:], uint64(tstamp))
	binary.LittleEndian.PutUint16(buf[8:], uint16(len(key)))
	binary.LittleEndian.PutUint16(buf[10:], valSz)
	binary.LittleEndian.PutUint32(buf[12:], uint32(valPos))
	buf = append(buf, []byte(key)...)
	return buf
}

// --------- KEYDIR STUFF ------------

// Creates the value that will be pointed to by a key in the in-memory
// keydir datastructure. I need to inspect this method closer, as I'm
// doing a lot of type casts
func createKeydirEntry(key, val []byte, fileId uint16, pos int64) [KEYDIR_ENTRY_SIZE]byte {
	buf := make([]byte, KEYDIR_ENTRY_SIZE)
	binary.LittleEndian.PutUint16(buf[:], fileId)
	binary.LittleEndian.PutUint16(buf[2:], uint16(len(val)))
	binary.LittleEndian.PutUint32(buf[4:], uint32(pos)) // Int overflow bug?
	tstamp := time.Now().Unix()
	binary.LittleEndian.PutUint64(buf[8:], uint64(tstamp))
	return [KEYDIR_ENTRY_SIZE]byte(buf)
}

func getKeydirFileId(buf *[KEYDIR_ENTRY_SIZE]byte) uint16 {
	return binary.LittleEndian.Uint16((*buf)[:])
}

func getValueInfoFromKeydir(b [KEYDIR_ENTRY_SIZE]byte) (uint16, uint16, uint32) {
	fileId := binary.LittleEndian.Uint16(b[:])
	valSz := binary.LittleEndian.Uint16(b[2:])
	valPos := binary.LittleEndian.Uint32(b[4:])
	return fileId, valSz, valPos
}

func parseFilePair(fp FilePair) (map[string][KEYDIR_ENTRY_SIZE]byte, [][]byte, error) {
	if fp.hint != "" {
		m, keyArr, err := parseUsingHint(fp)
		if err != nil {
			return parseMergeFile(fp.merge, fp.key)
		}

		return m, keyArr, nil
	} else {
		return parseMergeFile(fp.merge, fp.key)
	}
}

// This method differs from parseFilePair() above by returning k/v map
// and key slice instead of the raw keydir entry bytes. This method also
// Checks the timestamp of the entry and doesn't return it if it's not zeroed
func parseFilePairForCompaction(fp FilePair) (map[string][]byte, [][]byte, error) {
	return parseMergeForCompaction(fp.merge)
}

// Regular DB file with standard wire-protocol entries
func parseMergeFile(name string, id uint16) (map[string][KEYDIR_ENTRY_SIZE]byte, [][]byte, error) {
	f, err := os.Open(fmt.Sprintf("%s%s", BASE_DIR, name))
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		return nil, nil, err
	}

	buf := make([]byte, stat.Size())
	n, err := f.Read(buf)
	if err != nil {
		return nil, nil, err
	}

	if int64(n) != stat.Size() {
		return nil, nil, fmt.Errorf("expected read size, %d does not equal read: %d", stat.Size(), n)
	}

	r := make(map[string][KEYDIR_ENTRY_SIZE]byte, 0)
	keyArr := make([][]byte, 0)

	offset := 0
	for int64(offset) < stat.Size() {
		beforeKV := buf[offset : offset+KV_ENTRY_META_SIZE]
		keySz := getKvKeySize(&beforeKV)
		valSz := getKvValSize(&beforeKV)
		tmp := buf[offset : offset+KV_ENTRY_META_SIZE+keySz+int(valSz)]
		key := getKvKey(&tmp)
		tstamp := getKvTimestamp(&tmp)
		entry := make([]byte, 16)
		binary.LittleEndian.PutUint16(entry[:], id)
		binary.LittleEndian.PutUint16(entry[2:], uint16(valSz))
		binary.LittleEndian.PutUint32(entry[4:], uint32(offset))
		binary.LittleEndian.PutUint64(entry[8:], uint64(tstamp.Unix()))

		r[string(key)] = [KEYDIR_ENTRY_SIZE]byte(entry)
		keyArr = append(keyArr, key)

		offset = offset + keySz + int(valSz) + KV_ENTRY_META_SIZE
	}

	return r, keyArr, nil
}

func parseMergeForCompaction(fn string) (map[string][]byte, [][]byte, error) {
	f, err := os.Open(fmt.Sprintf("%s%s", BASE_DIR, fn))
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()

	stat, err := f.Stat()
	if err != nil {
		return nil, nil, err
	}

	buf := make([]byte, stat.Size())
	n, err := f.Read(buf)
	if err != nil {
		return nil, nil, err
	}

	if int64(n) != stat.Size() {
		return nil, nil, fmt.Errorf("expected read size, %d does not equal read: %d", stat.Size(), n)
	}

	kvMap := make(map[string][]byte, 0)
	keyArr := make([][]byte, 0)

	offset := 0
	for int64(offset) < stat.Size() {
		beforeKV := buf[offset : offset+KV_ENTRY_META_SIZE]
		keySz := getKvKeySize(&beforeKV)
		valSz := getKvValSize(&beforeKV)
		tmp := buf[offset : offset+KV_ENTRY_META_SIZE+keySz+int(valSz)]
		tstamp := getKvTimestamp(&tmp)
		key := getKvKey(&tmp)
		val := getKvVal(&tmp, keySz)

		if tstamp.IsZero() {
			kvMap[string(key)] = val
			keyArr = append(keyArr, key)
		}

		offset = offset + keySz + int(valSz) + KV_ENTRY_META_SIZE
	}

	return kvMap, keyArr, nil
}

// | key | => | file_id | value_sz | value_pos | tstamp |
//
//	[]byte => |  uint16  |  uint16  |   uint16  |  uint64 |
func parseUsingHint(fp FilePair) (map[string][KEYDIR_ENTRY_SIZE]byte, [][]byte, error) {
	f, err := os.Open(fmt.Sprintf("%s%s", BASE_DIR, fp.hint))
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()
	stat, err := f.Stat()
	if err != nil {
		return nil, nil, err
	}

	buf := make([]byte, stat.Size())
	n, err := f.Read(buf)
	if err != nil {
		return nil, nil, err
	}
	if int64(n) != stat.Size() {
		return nil, nil, fmt.Errorf("expected read size, %d does not equal read: %d", stat.Size(), n)
	}

	return_map := make(map[string][KEYDIR_ENTRY_SIZE]byte, 0)
	keyArr := make([][]byte, 0)

	offset := 0
	for int64(offset) < stat.Size() {
		// How many bytes do we need for one Hint entry?
		tmp := buf[offset : offset+HINT_FILE_META_SIZE]
		keySz := getHintEntryKeySize(&tmp)
		index := offset + HINT_FILE_META_SIZE + keySz
		tmp = buf[offset:index]
		key := getHintEntryKey(&tmp)
		valSz := getHintEntryValSize(&tmp)
		valPos := getHintEntryValPos(&tmp)
		tstamp := getHintEntryTimestamp(&tmp)

		entry := make([]byte, 16)
		binary.LittleEndian.PutUint16(entry[:], fp.key)
		binary.LittleEndian.PutUint16(entry[2:], uint16(valSz))
		binary.LittleEndian.PutUint32(entry[4:], uint32(valPos))
		binary.LittleEndian.PutUint64(entry[8:], tstamp)

		return_map[string(key)] = [KEYDIR_ENTRY_SIZE]byte(entry)
		keyArr = append(keyArr, key)

		offset = offset + HINT_FILE_META_SIZE + keySz
	}

	return return_map, keyArr, nil
}
