# Compaction 


## We should not be manually calling kv.Compact() 
> We really need to be monitoring the directory pro-actively and be greedy with running compaction


#### Greedy Approach
- Run a background thread dedicated to monitoring the directory size. As soon as we hit the limit, we run compaction