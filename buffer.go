package ldb

import (
	"fmt"
	"math/rand"
	"time"
	"unsafe"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

var src = rand.NewSource(time.Now().UnixNano())

type Buffer struct {
	keydir       Keydir
	activeFile   DB
	oldFiles     map[uint16]string
	newDbFiles   map[uint16]string
	newHintFiles map[uint16]string
}

func (b *Buffer) createNewKeydir() (map[string][KEYDIR_ENTRY_SIZE]byte, error) {
	newKeydir := make(map[string][KEYDIR_ENTRY_SIZE]byte, 0)
	// Let's create a new merge and hint
	// merge, hint := createHintAndMerge()
	// mergeBuf := make([]byte, 0)
	// hintBuf := make([]byte, 0)

	// for k, v := range b.keydir.data {

	// }

	return newKeydir, nil
}

func createHintAndMerge() (string, string) {
	// hash :=
	// rand.NewSource(new(maphash.Hash).Sum32())
	suffix := RandString()
	fullDb := fmt.Sprintf("%s%s%s", BASE_DIR, MERGE_FILE_PREPEND, suffix)
	fullHint := fmt.Sprintf("%s%s%s", BASE_DIR, HINT_FILE_PREPEND, suffix)

	return fullDb, fullHint
}

// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go
func RandString() string {
	n := 10
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return *(*string)(unsafe.Pointer(&b))
}
