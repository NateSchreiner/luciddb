package ldb

import (
	"fmt"
	"io/fs"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
	"unsafe"

	"github.com/howeyc/crc16"
)

type TestCase struct {
	input    map[string]string
	expected map[string]string
}

func TestTime(t *testing.T) {
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	m := makeTestMap(1_000_000)
	start := time.Now()
	for k, v := range m {
		err := kv.Put([]byte(k), []byte(v))
		if err != nil {
			log.Printf("Could not add key. err=%s\n", err.Error())
		}
	}
	duration := time.Since(start)

	fmt.Printf("Time Elapsed: %d\n", duration.Milliseconds())

	kv.Close()

}

func makeTestMap(amount int) map[string]string {
	r := make(map[string]string, 0)
	for i := 0; i < amount; i++ {
		key := makeRandomString(64)
		value := makeRandomString(64)
		r[key] = value
	}

	fmt.Printf("Made %d k/v's\n", len(r))

	return r
}

func startFresh(dir string) {
	_, err := os.Stat(dir)
	if err == nil {
		err := os.RemoveAll(dir)
		if err != nil {
			panic(err)
		}
		err = os.Mkdir(dir, 0777)
		if err != nil {
			panic(err)
		}
	}
}

func TestHintFileValidity(t *testing.T) {
	// Write one file (Make sure it's bigger than compact threshold)
	// Close KV
	// Re-open KV
	// Call compaction
	// Read hint file directly
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	_ = insertRandomPairs(1000, kv)
	kv.Close()

	final, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	err = final.Compact()
	if err != nil {
		t.Fatalf("error compacting. err=%s", err.Error())
	}

	// TODO:: Read hint file

}

func TestInMemoryKeydir(t *testing.T) {
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	_ = insertRandomPairs(1000, kv)
	kv.Close()

	kvTwo, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	_ = insertRandomPairs(1000, kvTwo)
	kvTwo.Close()

	final, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	defer final.Close()

	err = filepath.Walk(BASE_DIR, func(path string, info fs.FileInfo, err error) error {
		if !strings.Contains(info.Name(), ACTIVE_FILE_PREPEND) && !strings.Contains(BASE_DIR, info.Name()) {
			_ = checkFileAgainstKeydir(t, path, final)
			return nil
		}

		return nil
	})

	if err != nil {
		panic(err)
	}
}

func TestFileValidity(t *testing.T) {
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	planned := insertPlannedPairs(500, kv)
	kv.Close()

	kv2, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	plannedTwo := insertPlannedPairs(500, kv2)
	kv2.Close()

	kv3, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	plannedThree := insertPlannedPairs(500, kv3)
	kv3.Close()

	m := mergeMaps(planned, plannedTwo, plannedThree)

	err = filepath.Walk(BASE_DIR, func(path string, info fs.FileInfo, err error) error {
		if !strings.Contains(info.Name(), ACTIVE_FILE_PREPEND) && !strings.Contains(BASE_DIR, info.Name()) {
			f, err := os.Open(path)
			if err != nil {
				panic(err)
			}

			buf := make([]byte, info.Size())
			n, err := f.Read(buf)
			if err != nil {
				panic(err)
			}

			if int64(n) != info.Size() {
				t.Fatalf("Write %d did not match size %d\n", n, info.Size())
			}

			checkFile(t, buf, m)
		}

		return nil
	})

	if err != nil {
		panic(err)
	}
}

func checkFileAgainstKeydir(t *testing.T, path string, kv *KV) bool {
	f, err := os.Open(path)
	if err != nil {
		t.Fatal(err.Error())
	}
	stat, err := f.Stat()
	if err != nil {
		t.Fatalf(err.Error())
	}

	fileBuf := make([]byte, stat.Size())
	n, err := f.Read(fileBuf)
	if err != nil {
		t.Fatalf(err.Error())
	}

	if int64(n) != stat.Size() {
		t.Fatalf("Error read size: %d didn't match expected: %d", n, stat.Size())
	}
	hasher := crc16.New(crc16.IBMTable)
	fileName := filepath.Base(path)
	parts := strings.Split(fileName, "_")
	if len(parts) < 1 {
		t.Fatalf("could not parse filename: %s", fileName)
	}
	hasher.Write([]byte(parts[1]))
	fileId := hasher.Sum16()

	offset := uint32(0)
	for int(offset) < len(fileBuf) {
		buf := fileBuf[offset : offset+KV_ENTRY_META_SIZE]
		keySz := getKvKeySize(&buf)
		valSz := getKvValSize(&buf)

		buf = fileBuf[offset : int(offset)+KV_ENTRY_META_SIZE+keySz+int(valSz)]
		key := getKvKey(&buf)

		keydirEntry := kv.keydir.data[string(key)]
		kde := (*[KEYDIR_ENTRY_SIZE]byte)(unsafe.Pointer(&keydirEntry))
		// Do the bytes in kde match keydirEntry exactly?
		fId, vSz, valPos := getValueInfoFromKeydir(*kde)
		if fId != fileId {
			t.Fatalf("FileId's don't match. Expected=%d, got=%d", fileId, fId)
		}

		if vSz != valSz {
			t.Fatalf("valSize doesn't match expected. Expected=%d, got=%d", valSz, vSz)
		}

		if valPos != offset {
			t.Fatalf("ValPos does not match expected. Expected=%d, got=%d", offset, valPos)
		}

		offset = uint32(int(offset) + KV_ENTRY_META_SIZE + keySz + int(valSz))
	}

	return true
}

func checkFile(t *testing.T, buf []byte, m map[string]string) bool {
	offset := 0
	for offset < len(buf) {
		b := buf[offset : offset+KV_ENTRY_META_SIZE]
		keySz := getKvKeySize(&b)
		valSz := getKvValSize(&b)
		// This might not work!?
		b = buf[offset : offset+KV_ENTRY_META_SIZE+keySz+int(valSz)]
		key := getKvKey(&b)
		val := getKvVal(&b, keySz)

		v, ok := m[string(key)]
		if !ok {
			t.Fatalf("Couldn't find planned key: %s in plannedMap", key)
		}

		if v != string(val) {
			t.Fatalf("Retreived val, %s, doesn't match expected: %s", v, val)
		}

		if keySz != len(key) {
			t.Fatalf("Expected key size, %d, does not match computed: %d", len(key), keySz)
		}

		if int(valSz) != len(v) {
			t.Fatalf("Expected value size, %d, does not match computed: %d", len(v), valSz)
		}

		offset = offset + KV_ENTRY_META_SIZE + keySz + int(valSz)
	}

	return true
}

func TestOpen(t *testing.T) {
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	firstStore := insertRandomPairs(1000, kv)
	kv.Close()

	kv2, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	secondStore := insertRandomPairs(1000, kv2)
	kv2.Close()

	final, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	defer final.Close()

	// Check final for all the keys from first and second store
	maps := []map[string]string{firstStore, secondStore}
	m := mergeMaps(maps...)

	for k, v := range m {
		val, err := final.Get([]byte(k))
		if err != nil {
			t.Fatalf("Failed to get key: %s", k)
		}

		if string(val) != v {
			t.Fatalf("Val in DB, '%s' does not match expected, '%s' ", string(val), v)
		}
	}
}

func TestSimpleRemoveKey(t *testing.T) {
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	store := insertRandomPairs(100000, kv)

	keyArr := make([]string, 0)
	for key := range store {
		keyArr = append(keyArr, key)
	}

	// Remove odd number keys
	for i, key := range keyArr {
		if i%2 != 0 {
			err = kv.Delete([]byte(key))
			delete(store, key)
			if err != nil {
				fmt.Printf("Could not delete key: %s\n", key)
			}
		}
	}

	for i, key := range keyArr {
		v, err := kv.Get([]byte(key))
		if err != nil {
			if i%2 == 0 {
				t.Fatalf("could not get key: %s. err=%s\n", key, err.Error())
			} else {
				if len(v) != 0 && string(v) != "" {
					t.Fatalf("Found value that should be deleted. key=%s, val=%s", key, string(v))
				}
			}
		}

		if string(v) != store[key] {
			t.Fatalf("returned val: %s doesn't match expected: %s\n", string(v), store[key])
		}

	}
}

func TestSmallCompaction(t *testing.T) {
	// We can test it by creating two small KV's and removing the
	// Compaction limit
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	firstTestCase := createTestCase() // 4 k/v pairs

	for k, v := range firstTestCase.input {
		err := kv.Put([]byte(k), []byte(v))
		if err != nil {
			fmt.Printf("Error adding key: %s\n", k)
			delete(firstTestCase.expected, k)
		}
	}
	kv.Close()

	kvTwo, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	secondTestCase := createSecondTestCase() // 3 k/v pairs
	for k, v := range secondTestCase.input {
		err := kvTwo.Put([]byte(k), []byte(v))
		if err != nil {
			fmt.Printf("Error adding key: %s\n", k)
			delete(secondTestCase.expected, k)
		}
	}

	kvTwo.Close()

	final, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	err = final.Compact()
	if err != nil {
		t.Fatalf("compact error: %s", err.Error())
	}

	finalMap := mergeMaps(firstTestCase.expected, secondTestCase.expected)

	// Does the new file, and thus `final` KV have all keys?

	for k, v := range finalMap {
		val, err := final.Get([]byte(k))
		if err != nil {
			t.Fatalf("Failed to get key: %s", k)
		}

		if string(val) != v {
			t.Fatalf("Val in DB, '%s' does not match expected, '%s' ", string(val), v)
		}
	}

}

func TestCompactionNoLoss(t *testing.T) {
	startFresh(BASE_DIR)
	kv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	firstStore := insertRandomPairs(1000, kv)
	kv.Close()

	secondKv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	secondStore := insertRandomPairs(1000, secondKv)
	secondKv.Close()

	thirdKv, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}

	thirdStore := insertRandomPairs(1000, thirdKv)

	oldFiles := thirdKv.keydir.files
	thirdKv.Close()

	final, err := Open()
	if err != nil {
		t.Fatalf("Err opening DB: %s", err.Error())
	}
	defer final.Close()

	err = final.Compact()
	if err != nil {
		t.Fatalf("Err compacting. err=%s", err.Error())
	}

	m := mergeMaps([]map[string]string{firstStore, secondStore, thirdStore}...)

	idx := 0
	for k, v := range m {
		idx += 1
		val, err := final.Get([]byte(k))
		if err != nil {
			t.Fatalf("Failed to get key: %s", k)
		}

		if string(val) != v {
			fmt.Printf("Idx: %d\n", idx)
			t.Fatalf("Val in DB, '%s' does not match expected, '%s' ", string(val), v)
		}
	}

	// TODO:: Check file sizes? Check that old DB (merge) file is gone?
	// Check that the directory is actually under the compaciton limit
	for id, filename := range oldFiles {
		// Check in-memory and storage
		path := fmt.Sprintf("%s%s", BASE_DIR, filename)
		_, err := os.Stat(path)
		if err == nil {
			t.Fatalf("Old file: %s still exists", filename)
		}

		val, ok := final.keydir.files[id]
		if ok {
			t.Fatalf("error found file: %s in keydir", val)
		}
	}
}

func mergeMaps(maps ...map[string]string) map[string]string {
	if len(maps) < 2 {
		return maps[0]
	}

	base := maps[0]

	for _, m := range maps {
		for k, v := range m {
			base[k] = v
		}
	}

	return base
}

func TestSimpleWireProtocol(t *testing.T) {
	db := "small.db"
	_, err := os.Stat(db)
	if err == nil {
		os.Remove(db)
	}

	key := []byte("Small")
	value := []byte("large")

	kv := Create()

	kv.Put(key, value)

	val, err := kv.Get(key)
	if err != nil {
		t.Fatalf("Couldn't find key: %s", key)
	}

	if string(val) != string(value) {
		t.Fatalf(fmt.Sprintf("Value received: \"%s\" does not match", val))
	}

	kv.Close()
}

func TestWireProtocolOnlyAdds(t *testing.T) {
	_, err := os.Stat("./luciddb/active.db")
	if err == nil {
		os.Remove("./luciddb/active.db")
	}

	input := make(map[string]string, 0)
	expected := make(map[string]string, 0)
	input["test me out"] = "first test"
	input["Second key to test!!!"] = "heres another key to test"
	input["SHall we try another"] = "another value for the KV store"
	input["At someo point I should generate this input"] = "When will I get generated input"

	for k, v := range input {
		expected[k] = v
	}

	tc := &TestCase{
		input:    input,
		expected: expected,
	}

	kv := Create()
	for k, v := range tc.input {
		// fmt.Printf("Writing: key: %s, value: %s\n", k, v)
		err := kv.Put([]byte(k), []byte(v))
		if err != nil {
			t.Fatalf("error putting key: \"%s\".. err=%s", k, err)
		}
	}

	for k, v := range tc.expected {
		val, err := kv.Get([]byte(k))
		if err != nil {
			t.Fatalf("Error getting key: %s. err=%s\n", k, err.Error())
		}

		if v != string(val) {
			t.Fatalf("Expected val: '%s' does not match returned: '%s'", v, val)
		}
	}

	kv.Close()
}

func TestWireProtocolDeletes(t *testing.T) {
	tc := createTestCase()

	kv := Create()

	for k, v := range tc.input {
		err := kv.Put([]byte(k), []byte(v))
		if err != nil {
			t.Fatalf("error adding key: \"%s\".. err=%s", k, err.Error())
		}
	}

	deleteKeys := make(map[string]string, 0)
	deleteKeys["this should definitely be deleted"] = "Value1"
	deleteKeys["somone check this key"] = "value2"
	deleteKeys["here some more key"] = "for your value"
	// We have "noise" data. Should make this much noiser
	// Now add keys that we'll delete

	for k, v := range deleteKeys {
		err := kv.Put([]byte(k), []byte(v))
		if err != nil {
			t.Fatalf("error adding delete key: \"%s\", err=%s", k, err.Error())
		}
	}

	for k := range deleteKeys {
		err := kv.Delete([]byte(k))
		if err != nil {
			t.Fatalf("Error deleting key \"%s\"", k)
		}
	}

	// Now check everything
	for k, v := range tc.expected {
		val, err := kv.Get([]byte(k))
		if err != nil {
			t.Fatalf("Did not find a value for key \"%s\"", k)
		}
		if string(val) != v {
			t.Fatalf("Expected value \"%s\" does not match received \"%s\"", v, val)
		}
	}

	for k := range deleteKeys {
		val, err := kv.Get([]byte(k))
		if err == nil {
			t.Fatalf("Key \"%s\" Should be deleted", k)
		}
		if len(val) != 0 {
			t.Fatalf("Found value: \"%s\" for deleted key: \"%s\"", val, k)
		}
	}
}

func TestWireProtocolAddsAndUpdates(t *testing.T) {
	tc := createTestCase()
	kv := Create()

	for k, v := range tc.input {
		err := kv.Put([]byte(k), []byte(v))
		if err != nil {
			t.Fatalf("failure adding key: \"%s\" err=%s", k, err)
		}
	}

	for k, v := range tc.expected {
		val, err := kv.Get([]byte(k))
		if err != nil {
			t.Fatalf("Error getting key: %s. err=%s\n", k, err.Error())
		}

		if v != string(val) {
			t.Fatalf("Expected val: '%s' does not match returned: '%s'", v, val)
		}
	}
}

func makeRandomString(size int) string {
	var runes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	b := make([]rune, size)
	for i := range b {
		b[i] = runes[rand.Intn(len(runes))]
	}

	return string(b)
}

func insertPlannedPairs(amount int, kv *KV) map[string]string {
	r := make(map[string]string, 0)
	keys := make([]string, 0)
	keys = append(keys, "firstKey")
	keys = append(keys, "secondKey")
	keys = append(keys, "thirdKey")
	keys = append(keys, "fourthKey")

	vals := make([]string, 0)
	vals = append(vals, "some knda val")
	vals = append(vals, "this is our value")
	vals = append(vals, "how about this value")
	vals = append(vals, "tooo much value you fish")

	for j := 0; j < amount; j++ {
		for i := 0; i < 4; i++ {
			err := kv.Put([]byte(keys[i]), []byte(vals[i]))
			if err != nil {
				fmt.Printf("Could not add key: %s, err=%s\n", keys[i], err.Error())
				continue
			}
			r[keys[i]] = vals[i]
		}
	}

	return r
}

func insertRandomPairs(amount int, kv *KV) map[string]string {
	store := make(map[string]string, 0)
	for i := 0; i < amount; i++ {
		key := makeRandomString(64)
		val := makeRandomString(64)
		store[key] = val
		err := kv.Put([]byte(key), []byte(val))
		if err != nil {
			fmt.Printf("Could not add key: %s, err=%s\n", key, err.Error())
			delete(store, key)
		}
	}

	return store
}

// Creates 4 K/V Pairs to insert into KV
func createTestCase() TestCase {
	input := make(map[string]string, 0)
	expected := make(map[string]string, 0)
	input["test me out"] = "first test"
	input["Second key to test!!!"] = "heres another key to test"
	input["SHall we try another"] = "another value for the KV store"
	input["At someo point I should generate this input"] = "When will I get generated input"
	input["test me out"] = "This is the true value"
	input["At someo point I should generate this input"] = "This also has a new value"

	for k, v := range input {
		expected[k] = v
	}

	expected["test me out"] = "This is the true value"
	expected["At someo point I should generate this input"] = "This also has a new value"

	tc := &TestCase{
		input:    input,
		expected: expected,
	}

	return *tc
}

// Creates 3 k/v pairs to add to KV
func createSecondTestCase() TestCase {
	input := make(map[string]string, 0)
	expected := make(map[string]string, 0)
	input["FirstKeyForCompaction"] = "secondValueForCompaction"
	input["Morekeysmorekeysmorekyes"] = "AnotherValueForCompactionTests"
	input["keykeykeykeykeykeykey"] = "HowAboutAnotherValueforCompactionTestsNow??"

	for k, v := range input {
		expected[k] = v
	}

	expected["FirstKeyForCompaction"] = "secondValueForCompaction"
	expected["Morekeysmorekeysmorekyes"] = "AnotherValueForCompactionTests"
	expected["keykeykeykeykeykeykey"] = "HowAboutAnotherValueforCompactionTestsNow??"

	tc := &TestCase{
		input:    input,
		expected: expected,
	}

	return *tc
}
