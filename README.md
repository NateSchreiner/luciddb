# LucidDB

### Lucid 
#### adjective
1. Expressed clearly; easy to understand.



| Branch |         Pipeline        |
|--------|-------------------------|
|  main  | ![pipeline status](https://gitlab.com/NateSchreiner/luciddb/badges/main/pipeline.svg) |




## Educational Key/Value store based on Bitcask
> https://riak.com/assets/bitcask-intro.pdf


##### Benchmarking performance 
> https://github.com/Percona-Lab/go-tpcc

https://en.wikipedia.org/wiki/TPC-C


##### SuRF Practical Range Query filtering with Fast succinct Tries
> https://www.cs.cmu.edu/~pavlo/papers/mod601-zhangA-hm.pdf

#### References
https://medium.com/@pradeepsinghbiet/bitc-db-a-key-value-store-based-on-bitcask-storage-format-b42e413a5704