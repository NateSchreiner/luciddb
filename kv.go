package ldb

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/howeyc/crc16"
)

// KV Pair Wire-protocol:
// TSTAMP = Tombstone timestamp.  0 value for NOT deleted
// | CRC | TSTAMP | KSZ | VSZ | Key | Val |
//
//	4B      8B     2B    2B   [u8]  [u8]
const KV_ENTRY_META_SIZE = 16

// | key | => | file_id | value_sz | value_pos | tstamp |
//
//	[]byte => |  uint16  |  uint16  |   uint32  |  uint64 |
const KEYDIR_ENTRY_SIZE = 16

const COMPACT_MEM_LIMIT = 1

// const COMPACT_MEM_LIMIT = 4096 << 4 // 4096 * (2*4) = 65536

const BASE_DIR = ".luciddb/"
const MERGE_FILE_PREPEND = "merged_"
const HINT_FILE_PREPEND = "hint_"
const ACTIVE_FILE_PREPEND = "active_"

type KV struct {
	db     DB
	keydir Keydir
}

func (kv *KV) Length() int {
	return len(kv.keydir.data)
}

type DB struct {
	file *os.File
	name string
	id   uint16
}

type Keydir struct {
	data  map[string][KEYDIR_ENTRY_SIZE]byte
	files map[uint16]string
	// Keydir wide mutex, probably should be refactors to record granularity
	mu sync.Mutex
}

func (kd *Keydir) addFile(id uint16, name string) {
	kd.files[id] = name
}

func getKeyFromFilename(name string) (uint16, error) {
	parts := strings.Split(name, "_")
	if len(parts) < 1 {
		return 0, fmt.Errorf("could not parse filename: %s", name)
	}
	hasher := crc16.New(crc16.IBMTable)
	hasher.Write([]byte(parts[1]))

	return hasher.Sum16(), nil
}

func Create() *KV {
	suffix := RandString()
	file := fmt.Sprintf("%s%s", ACTIVE_FILE_PREPEND, suffix)
	_, err := os.Stat(BASE_DIR)
	if err != nil {
		err := os.Mkdir(BASE_DIR, 0777)
		if err != nil {
			panic(err)
		}
	}

	var f *os.File
	_, err = os.Stat(fmt.Sprintf("%s%s", BASE_DIR, file))
	if err == nil {
		err = os.Remove(fmt.Sprintf("%s%s", BASE_DIR, file))
		if err != nil {
			panic(err)
		}
	}

	f, err = os.OpenFile(fmt.Sprintf("%s%s", BASE_DIR, file), os.O_CREATE|os.O_APPEND|os.O_RDWR, 0666)
	if err != nil {
		log.Fatalf("Error opening DB. err=%s", err)
	}

	kd := Keydir{
		data:  make(map[string][KEYDIR_ENTRY_SIZE]byte, 0),
		files: make(map[uint16]string, 0),
	}

	id, err := getKeyFromFilename(file)
	if err != nil {
		panic(err)
	}

	kd.addFile(id, file)

	activeFile := &DB{
		name: file,
		id:   id,
		file: f,
	}

	kv := &KV{
		db:     *activeFile,
		keydir: kd,
	}

	return kv
}

func Open() (*KV, error) {
	kv := &KV{}
	keydir := Keydir{}
	_, err := os.Stat(BASE_DIR)
	if err != nil {
		err := os.Mkdir(BASE_DIR, 0777)
		if err != nil {
			return nil, err
		}
	}
	files := createFileList(BASE_DIR)
	keydirFiles := make(map[uint16]string, 0)

	if keydir.data == nil {
		keydir.data = make(map[string][KEYDIR_ENTRY_SIZE]byte, 0)
	}

	// IF the files are sorted, we can just parse the files as i
	for _, fp := range files {
		if fp.merge == "" {
			panic(fmt.Sprintf("Null merge file? Key=%d\n", fp.key))
		}

		keydirFiles[fp.key] = fp.merge // Should always have a merge file
		keydirEntries, _, err := parseFilePair(fp)
		if err != nil {
			// What happens now? Couldn't parse a file?
			panic(err)
		}
		// TODO:: Change this to iterate through keyArr returned from parseFilePair()
		for key, val := range keydirEntries {
			_, ok := keydir.data[key]
			if !ok {
				keydir.data[key] = val
			}
		}
	}

	keydir.files = keydirFiles

	kv.keydir = keydir

	suffix := RandString()
	fileName := fmt.Sprintf("%s%s%s", BASE_DIR, ACTIVE_FILE_PREPEND, suffix)
	hasher := crc16.New(crc16.IBMTable)
	hasher.Write([]byte(suffix))

	f, err := os.Create(fileName)
	if err != nil {
		return nil, err
	}
	db := DB{
		name: fileName,
		id:   hasher.Sum16(),
		file: f,
	}

	kv.db = db
	return kv, nil
}

// We want to run this concurrently with other KV store operations.
// This means, we almost need to lock each record we touch here?
// Also means, we can't just create a new keydir and assign it to the KV,
// because some new k/v pairs may be added while we compact. We must merge
// the two keydir's together, making sure to pay attention to the most recent
// value.
func (kv *KV) Compact() error {
	currSize, err := dirSize(BASE_DIR)
	if err != nil {
		return err
	}

	if currSize < COMPACT_MEM_LIMIT {
		return nil
	}

	files := createFileList(BASE_DIR)

	mergeFile, fileId, err := createNewFileForCompaction()
	mergedFileName := mergeFile.Name()
	suffix := strings.Split(mergedFileName, "_")[1]
	if err != nil {
		return err
	}
	defer mergeFile.Close()

	hintFile, err := createHintFile(suffix)
	if err != nil {
		return err
	}
	defer hintFile.Close()

	newFileOffset := 0
	for _, filePair := range files {
		kvMap, keyArr, err := parseFilePairForCompaction(filePair)
		if err != nil {
			return err
		}

		for _, key := range keyArr {
			kvEntry, hintEntry, kdEntry := createCompactionEntries(key, kvMap[string(key)], fileId, int64(newFileOffset))
			n, err := mergeFile.Write(kvEntry)
			if err != nil {
				return err
			}
			newFileOffset += n

			if n != len(kvEntry) {
				return fmt.Errorf("full kvEntry not written. Wrote=%d, expected=%d", n, len(kvEntry))
			}

			_, err = hintFile.Write(hintEntry)
			if err != nil {
				fmt.Printf("Error writing hint entry. err=%s\n", err.Error())
			}

			kv.keydir.mu.Lock()
			kv.keydir.data[string(key)] = kdEntry
			kv.keydir.mu.Unlock()
		}
	}

	kv.keydir.mu.Lock()
	kv.keydir.files = make(map[uint16]string, 0)
	kv.keydir.files[fileId] = mergedFileName
	kv.keydir.mu.Unlock()

	err = deleteAll(fmt.Sprintf("%s%s", MERGE_FILE_PREPEND, suffix), fmt.Sprintf("%s%s", HINT_FILE_PREPEND, suffix))
	if err != nil {
		return err
	}

	return nil
}

// Creates a kvEntry and its' corresponding hint file entry
func createCompactionEntries(key, val []byte, fileId uint16, offset int64) ([]byte, []byte, [KEYDIR_ENTRY_SIZE]byte) {
	kvEntry := createKvEntry(key, val)

	hintEntry := createHintEntry(string(key), offset, uint16(len(val)))

	keydirEntry := createKeydirEntry(key, val, fileId, offset)

	return kvEntry, hintEntry, keydirEntry
}

func createNewFileForCompaction() (*os.File, uint16, error) {
	hasher := crc16.New(crc16.IBMTable)
	suffix := RandString()
	_, err := hasher.Write([]byte(suffix))
	if err != nil {
		return nil, 0, err
	}
	newFileId := hasher.Sum16()

	mergeFilename := fmt.Sprintf("%s%s%s", BASE_DIR, MERGE_FILE_PREPEND, suffix)
	f, err := os.Create(mergeFilename)
	if err != nil {
		return nil, 0, err
	}

	return f, newFileId, nil
}

func createHintFile(suffix string) (*os.File, error) {
	fn := fmt.Sprintf("%s%s%s", BASE_DIR, HINT_FILE_PREPEND, suffix)
	f, err := os.Create(fn)
	if err != nil {
		return nil, err
	}
	return f, nil
}
func deleteAll(except string, exceptTwo string) error {
	err := filepath.Walk(BASE_DIR, func(path string, info fs.FileInfo, err error) error {
		if !strings.Contains(info.Name(), ACTIVE_FILE_PREPEND) &&
			!strings.Contains(BASE_DIR, info.Name()) &&
			!strings.Contains(info.Name(), HINT_FILE_PREPEND) {

			if info.Name() != except && info.Name() != exceptTwo {
				return os.Remove(path)
			}

		}

		return nil
	})

	return err
}

func dirSize(dir string) (int64, error) {
	var size int64
	err := filepath.Walk(dir, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		}

		return err
	})

	return size, err
}

func (kv *KV) Close() {
	kv.db.file.Close()

	suffix := RandString()
	new := fmt.Sprintf("%s%s%s", BASE_DIR, MERGE_FILE_PREPEND, suffix)

	err := os.Rename(kv.db.file.Name(), new)
	if err != nil {
		panic(err)
	}
}

// I really shouldn't be opening a file everytime I get a key (perf)
func (kv *KV) Get(key []byte) ([]byte, error) {
	kv.keydir.mu.Lock()
	bytes, ok := kv.keydir.data[string(key)]
	kv.keydir.mu.Unlock()

	if !ok {
		return []byte{}, errors.New("did not find key in keydir")
	}

	fileId, valSz, valPos := getValueInfoFromKeydir(bytes)
	fName, ok := kv.keydir.files[fileId]

	if ok || fileId == kv.db.id {
		var wanted *os.File
		if kv.db.id == fileId {
			wanted = kv.db.file
		} else {
			// Check if fName contains BASE_DIR
			var filePath string
			if strings.Contains(fName, BASE_DIR) {
				filePath = fName
			} else {
				filePath = fmt.Sprintf("%s%s", BASE_DIR, fName)
			}
			f, err := os.Open(filePath)
			if err != nil {
				return []byte{}, err
			}
			defer f.Close()
			wanted = f
		}

		// valPos only points to the start of the entry
		realPos := int(valPos) + KV_ENTRY_META_SIZE + len(key)
		wanted.Seek(int64(realPos), 0)

		buf := make([]byte, valSz)
		read := uint16(0)
		for read < valSz {
			n, err := wanted.Read(buf)
			if err != nil {
				return []byte{}, err
			}
			read = read + uint16(n)
		}

		return buf, nil
	}
	return []byte{}, fmt.Errorf("could not find key: %s in keydir", key)
}

func (kv *KV) Put(key, val []byte) error {
	stat, err := kv.db.file.Stat()
	if err != nil {
		return err
	}

	entry := createKvEntry(key, val)
	keydirEntry := createKeydirEntry(key, val, kv.db.id, stat.Size())
	n, err := kv.db.file.Write(entry)
	if err != nil {
		return err
	}

	if n != len(entry) {
		return err
	}

	kv.keydir.mu.Lock()
	kv.keydir.data[string(key)] = [KEYDIR_ENTRY_SIZE]byte(keydirEntry)
	kv.keydir.mu.Unlock()

	return nil
}

// TODO: This isn't right.  We want to write a timestamp to KvEntry
func (kv *KV) Delete(key []byte) error {
	entry := createDeletedKvEntry(key, []byte{})
	_, err := kv.db.file.Write(entry)
	if err != nil {
		return err
	}

	delete(kv.keydir.data, string(key))
	return nil
}
