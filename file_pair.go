package ldb

import (
	"io/fs"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"github.com/howeyc/crc16"
)

type FilePair struct {
	key      uint16
	merge    string
	hint     string
	_modTime time.Time
}

func createFileList(dir string) []FilePair {
	r := make([]FilePair, 0)
	m := make(map[uint16]FilePair, 0)
	hasher := crc16.New(crc16.IBMTable)
	err := filepath.WalkDir(dir, func(path string, d fs.DirEntry, err error) error {
		// if !strings.Contains(d.Name(), ACTIVE_FILE_PREPEND) && !strings.Contains(dir, d.Name()) {
		if !strings.Contains(dir, d.Name()) && !strings.Contains(ACTIVE_FILE_PREPEND, d.Name()) {
			// Hash
			parts := strings.Split(d.Name(), "_")
			hasher.Reset()
			hasher.Write([]byte(parts[1]))
			id := hasher.Sum16()
			val, ok := m[id]
			if !ok {
				info, err := d.Info()
				if err != nil {
					return err
				}

				modTime := info.ModTime()
				m[id] = createFilePair(d.Name(), id, modTime)
			} else {
				fp := addFileToPair(d.Name(), val)
				r = append(r, fp)
				delete(m, id)
			}
		}

		return nil
	})

	// Now any files left in m, don't have a pair to go along with, and haven't been added
	if len(m) > 0 {
		for _, pair := range m {
			r = append(r, pair)
		}
	}

	if err != nil {
		panic(err)
	}

	// Sort slice before returning it
	sort.Slice(r, func(i, j int) bool {
		return r[i]._modTime.After(r[j]._modTime)
	})

	return r
}

func addFileToPair(filename string, fp FilePair) FilePair {
	if strings.Contains(filename, MERGE_FILE_PREPEND) {
		fp.merge = filename
	} else if strings.Contains(filename, HINT_FILE_PREPEND) {
		fp.hint = filename
	}

	return fp
}

func createFilePair(name string, id uint16, mod time.Time) FilePair {
	f := FilePair{}

	if strings.Contains(name, HINT_FILE_PREPEND) {
		f.hint = name
		f.merge = ""
	} else if strings.Contains(name, MERGE_FILE_PREPEND) || strings.Contains(name, ACTIVE_FILE_PREPEND) {
		f.hint = ""
		f.merge = name
		f._modTime = mod
	} else {
		return f
	}

	f.key = id
	return f
}
